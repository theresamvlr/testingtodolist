package com.theresa;

public class Task {
    private static final String DONE = "[DONE]";
    private static final String NOT_DONE = "[NOT DONE]";

    private String name;
    private String category;
    private int id;
    private boolean status;

    public Task(String name, String category, int id, boolean status)
    {
        this.id = id;
        this.category = category;
        this.name = name;
        this.status = status;
    }

    String getCategory(){
        return this.category;
    }

    String getName(){
        return this.name;
    }

    int getId(){
        return this.id;
    }

    public String getStatus()
    {
        return status ? DONE : NOT_DONE;
    }

    public void changeStatus(){
        if(this.getStatus().equals(DONE)){
            this.status = false;
        } else if(this.getStatus().equals(NOT_DONE)){
            this.status = true;
        }
    }

    boolean isId(int i){
        return this.id == i;
    }

}
