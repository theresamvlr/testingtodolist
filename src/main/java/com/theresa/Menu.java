package com.theresa;

import java.util.Scanner;

class Menu {
    private TodoList todoList = new TodoList();

    private Scanner sc = new Scanner(System.in);

    private void insertDummyTask() {
        todoList.addTask("Ngoding", "Kuliah",true);
        todoList.addTask("Makan", "Rumah",false);
        todoList.addTask("Tidur", "Rumah",true);
    }

    void run() {
        int choice = 0;
        insertDummyTask();

        do {
            displayMenu();
            if (sc.hasNextInt()) {
                choice = sc.nextInt();
            }
            sc.nextLine();

            switch (choice) {
                case 1:
                    addTask();
                    break;
                case 2:
                    todoList.displayTodoList();
                    break;
                case 3:
                    viewByCategory();
                    break;
                case 4:
                    markTaskDoneById();
                    break;
                case 5:
                    deleteTaskById();
                    break;
            }
        } while (choice != 6);
    }

    private void displayMenu() {
        System.out.println("DIY YOUR TODO LIST");
        System.out.println("1. Add Task");
        System.out.println("2. View All Tasks");
        System.out.println("3. View By Category");
        System.out.println("4. Change Status");
        System.out.println("5. Delete Task");
        System.out.println("6. Exit");
        System.out.println("Input your choice:");
    }

    private void addTask() {
        String name = null;
        String category = null;
        String status = null;

        System.out.println("Input the name of task:");
        if (sc.hasNext()) {
            name = sc.nextLine();
        }

        System.out.println("Input the name of category:");
        if (sc.hasNext()) {
            category = sc.nextLine();
        }

        System.out.println("Input status: write DONE, or NOT DONE");
        if (sc.hasNext()) {
            status = sc.nextLine();
        }
        if (name != null && category != null && status != null) {
            boolean boolStatus= status.equals("DONE");
            todoList.addTask(name, category, boolStatus);
        }
    }

    private void markTaskDoneById() {
        int id;

        todoList.displayTodoList();
        System.out.println();
        System.out.println("Input the id of the task you want to change:");
        id = sc.nextInt();
        sc.nextLine();
        todoList.changeTaskStatus(id);
        todoList.displayTodoList();
        System.out.println();
    }

    private void deleteTaskById(){
        int id;

        todoList.displayTodoList();
        System.out.println();
        System.out.println("Input the id of the task you want to delete:");
        id = sc.nextInt();
        sc.nextLine();
        todoList.deleteTaskFromTodoList(id);
        todoList.displayTodoList();
        System.out.println();
    }

    private void viewByCategory(){
        String category = null;

        System.out.println("Input the category you want to display:");
        if (sc.hasNext()) {
            category = sc.nextLine();
        }
        System.out.println(todoList.displayTodoListByCategory(category));
    }
}
