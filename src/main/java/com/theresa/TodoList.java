package com.theresa;

import java.util.ArrayList;

public class TodoList {
    private ArrayList<Task> taskArrayList = new ArrayList<>();
    private int taskArrayListIdIncrement = 0;

    public int getTasksSize(){
        return taskArrayList.size();
    }

    public void addTask(String name, String category, boolean status) {
        this.taskArrayListIdIncrement++;
        int id = this.taskArrayListIdIncrement;
        Task task = new Task(name, category, id, status);
        taskArrayList.add(task);
    }

    public String getAllTask(){
        StringBuilder ret;
        ret = new StringBuilder();
        for (Task task : taskArrayList) {
            ret.append(task.getId());
            ret.append(". ");
            ret.append(task.getCategory());
            ret.append(": ");
            ret.append(task.getName());
            ret.append(" ");
            ret.append(task.getStatus()).append("\n");
        }
        return ret.toString();
    }

    void displayTodoList(){
        System.out.print(getAllTask());
    }

    public String getTask(int id){
        Task task = taskArrayList.get(id-1);
        return task.getName() + " (" + task.getCategory() + ") " + task.getStatus() + "\n";
    }

    public void changeTaskStatus(int id){
        Task task = null;
        for (Task value : taskArrayList) {
            if (value.getId() == id) {
                task = value;
            }
        }
        if(task == null){
            System.out.println("ID NOT FOUND");
        } else{
            task.changeStatus();
        }
    }

    public void deleteTaskFromTodoList(int id){
        int index = -1;
        for (int i = 0; i< taskArrayList.size(); i++) {
            if (taskArrayList.get(i).isId(id)) {
                index = i;
            }
        }
        if(index == -1){
            System.out.println("ID NOT FOUND");
        } else{
            taskArrayList.remove(index);
        }
    }

    public String displayTodoListByCategory(String category){
        StringBuilder ret;
        ret = new StringBuilder();
        for (Task task : taskArrayList)
            if (task.getCategory().equals(category)) {
                ret.append(task.getName());
                ret.append(" ");
                ret.append(task.getStatus());
                ret.append("\n");
            }
        return ret.toString();
    }
}