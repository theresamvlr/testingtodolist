package com.theresa;

import org.junit.Test;

import static org.junit.Assert.*;

public class TodoListTest {

    @Test
    public void testAddTask() {
        TodoList todoList = new TodoList();
        int expectedSize = 3;

        todoList.addTask("Ngoding", "Kuliah", true);
        todoList.addTask("Makan", "Rumah", false);
        todoList.addTask("Tidur", "Rumah",true);

        assertEquals(expectedSize, todoList.getTasksSize());
    }

    @Test
    public void testGetAllTask(){
        TodoList todoList = new TodoList();

        todoList.addTask("Ngoding", "Kuliah",true);

        assertEquals("1. Kuliah: Ngoding [DONE]\n", todoList.getAllTask());
    }

    @Test
    public void testGetTask(){
        TodoList todoList = new TodoList();

        todoList.addTask("Ngoding", "Kuliah",true);

        assertEquals("Ngoding (Kuliah) [DONE]\n", todoList.getTask(1));
    }
    @Test
    public void testChangeStatusTodoListTask(){
        TodoList todoList = new TodoList();

        todoList.addTask("Ngoding", "Kuliah",false);
        todoList.changeTaskStatus(1);

        assertEquals("Ngoding (Kuliah) [DONE]\n", todoList.getTask(1));
    }

    @Test
    public void testDeleteTodoListTask(){
        TodoList todoList = new TodoList();

        todoList.addTask("Ngoding", "Kuliah", true);
        todoList.addTask("Makan", "Rumah", false);
        todoList.addTask("Tidur", "Rumah",true);
        todoList.deleteTaskFromTodoList(2);

        assertEquals("1. Kuliah: Ngoding [DONE]\n3. Rumah: Tidur [DONE]\n", todoList.getAllTask());
    }

    @Test
    public void testDisplayByCategoryTodoListTask(){
        TodoList todoList = new TodoList();

        todoList.addTask("Ngoding", "Kuliah", true);
        todoList.addTask("Makan", "Rumah", false);
        todoList.addTask("Tidur", "Rumah",true);

        assertEquals("Makan [NOT DONE]\nTidur [DONE]\n", todoList.displayTodoListByCategory("Rumah"));
    }
}