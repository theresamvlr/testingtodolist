package com.theresa;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void testChangeStatus(){
        Task task = new Task("Ngoding", "Kuliah", 1, false);

        task.changeStatus();

        assertEquals("[DONE]",task.getStatus());
    }
}